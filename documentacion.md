# Índice

1. [Introducción](#1-introducción)
	1. [Descripción del proyecto](#11-descripción-del-proyecto)
2. [¿Qué es PowerDNS?](#2-¿qué-es-powerdns?)
	1. [Backends](#21-backends)
	2. [WebFrontend](#22-webfronted)
	3. [Servidores y sus diferencias](#23-servidores-y-sus-diferencias)
3. [Instalaciones](#3-instalaciones)
	1. [Instalación servidor autoritativo](#31-instalación-servidor-autoritativo)
	2. [Instalación servidor recursor](#32-instalación-servidor-recursor)
4. [Configuraciones](#4-configuraciones)
	1. [Escenarios](#41-escenarios)
	2. [Configuración Maestro / Esclavo con backend MySQL](#42-configuración-maestro-esclavo-con-backend-mysql)
	3. [Configuración con backend LDAP](#43-configuración-con-backend-ldap)
	4. [Configuración de PowerAdmin](#44-configuración-de-poweradmin)
	5. [Delegación de subdominios](#45-delegación-de-subdominios)
5. [Conclusión](#5-conclusión)
6. [Siguientes Pasos](#6-siguientes-pasos)
7. [Webgrafía](#7-webgrafía)

# 1. Introducción

## 1.2 Descripción del proyecto
Vamos a instalar el servidor autoritativo **(pdns-server)** en el cuál veremos varias configuraciones, entre ellas configuraremos 2 nodos, uno de ellos será el maestro y otro será el esclavo, en el cuál vamos a transferir las zonas. Usaremos como motor de base de datos **MySQL**. También configuraremos powerdns como motor de base de datos **LDAP** el cuál nos responderá las peticiones que realizaremos a dicho servidor. También mostraremos el uso de su panel web **Poweradmin** para administrar dicho servidor desde la interfaz web. Finalmente para concluir, realizaremos una delegación de subdominios, en el cuál realizaremos preguntas recursivas en el nodo el cuál será autoritativo de dicha zona. 

# 2. ¿Qué es PowerDNS?
**PowerDNS** es un servidor DNS escrito en C++ con licencia GPL. Funciona en todos los sistemas derivados de UNIX y nos permite guardar la información en diversos motores de bases de datos desde los simples archivos de texto como BIND hasta base de datos relacionales como MySQL, PostgreSQL, LDAP...

## 2.1 Backends
Los backends son los diferentes motores de bases de datos soportados por PowerDNS en los cuales vamos a guardar la información. En mi caso usaré MySQL y LDAP. También soporta los siguientes:
* BIND 					(bind)
* Generic Mysql 		(gmysql)
* Generic ODBC 			(godbc)
* Generic Postgresql 	(gpgsql)
* Generic SQLite3		(gsqlite3)
* GeoIP					(geoip)
* LDAP 					(ldap)
* LMDB					(lmdb)
* Lua2					(lua2)
* Pipe					(pipe)
* Random				(radom)
* Remote 				(remote)
* TinyDNS				(tinydns)

Para instalar cualquiera de éstos backends ejecutaremos
~~~
sudo apt install pdns-backend-nombrebackend
~~~

## 2.2 WebFrontend
Existen diversos paneles web. En mi caso voy a usar PowerDNS-Admin
* PowerAdmin
* PDNS Manager
* DjangoPowerDNS
* Opera DNS UI
* nsedit

## 2.3 Servidores y sus diferencias
PowerDNS está formado opr dos componentes:
* Autoritativo: **pdns**
* Recursivo: **pdns-recursor**

El servidor DNS autoritativo es la fuente que proporciona la información para la resolución de nombres para un dominio en particular y pueden ser de dos tipos: **primarios** y **secundarios**. En cuanto al servidor recursivo, nos proporciona un sistema de almacenamiento en caché para así acelerar la resolución de nombres.
El servidor recursivo envía la consulta al servidor de nombres autorizado para resolver los detalles del registro solicitado.

# 3. Instalaciones
## 3.1 Instalación servidor autoritativo
**Autoritativo** => `sudo apt install -y pdns-server`

## 3.2 Instalación servidor recursor
**Recursivo** => `sudo apt install -y pdns-recursor`

# 4. Configuraciones
## 4.1 Escenarios
* Backend mysql + maestro / esclavo => `2 nodos (nodo1 y nodo2). Nodo1 => 192.168.1.147 ; Nodo2 => 192.168.1.137`
* Backend ldap => `1 nodo (pdnsldap). Pdnsldap => 192.168.1.109`
* Webfrotend => `1 nodo (poweradmin). Poweradmin => 192.168.1.89`
* Delegación subdominios `2 nodos (nodo1 y nodo3). Nodo3 => 192.168.1.136`

## 4.2 Configuración Maestro / Esclavo con backend MySQL

**Instalación en ambos nodos**

Instalamos el paquete de la base de datos mariadb
~~~
root@nodo1:~# apt install mariadb-server -y
root@nodo2:~# apt install mariadb-server -y
~~~

Instalamos el servidor powerdns y el backend para mysql 
~~~
root@nodo1:~# apt install -y pdns-server pdns-backend-mysql 
root@nodo2:~# apt install -y pdns-server pdns-backend-mysql 
~~~

`Nos aparecerá una ventana preguntando si queremos que pdns-backend-mysql configure la base de datos, seleccionamos no.`

Vamos a crear una base de datos, un usuario y asignaremos los privilegios necesarios sobre la base de datos creada
~~~
MariaDB [(none)]> create database powerdns;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> grant all on powerdns.* to 'powerdns'@'localhost' identified by 'powerdns';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.00 sec)
~~~

Cargamos el fichero con el esquema de powerdns en la base de datos creada anteriormente
~~~
root@nodo1:~# mysql -u powerdns -p powerdns < /usr/share/dbconfig-common/data/pdns-backend-mysql/install/mysql 
root@nodo2:~# mysql -u powerdns -p powerdns < /usr/share/dbconfig-common/data/pdns-backend-mysql/install/mysql 
~~~

Comprobamos que el esquema se ha cargado correctamente
~~~
MariaDB [powerdns]> show tables;
+--------------------+
| Tables_in_powerdns |
+--------------------+
| comments           |
| cryptokeys         |
| domainmetadata     |
| domains            |
| records            |
| supermasters       |
| tsigkeys           |
+--------------------+
7 rows in set (0.002 sec)
~~~

Editamos el fichero de configuración /etc/powerdns/pdns.d/pdns.local.gmysql.conf para establecer la comunicación entre powerdns y mariadb
~~~
# MySQL Configuration
#
# Launch gmysql backend
launch+=gmysql

# gmysql parameters
gmysql-host=localhost
gmysql-port=3306
gmysql-dbname=powerdns
gmysql-user=powerdns
gmysql-password=powerdns
gmysql-dnssec=yes
# gmysql-socket=
~~~

**Configuración Nodo 1 (Maestro)**

Editamos el fichero de configuración /etc/powerdns/pdns.con
~~~
 allow-axfr-ips=192.168.1.0/24
 config-dir=/etc/powerdns
 default-soa-mail=jm.root.mail
 default-soa-name=nodo1.prueba.org
 disable-axfr=no
 include-dir=/etc/powerdns/pdns.d
 launch=
 local-address=192.168.1.147
 local-port=53
 loglevel=4
 master=yes
 setgid=pdns
 setuid=pdns
 slave=no
~~~

Accedemos a la base de datos que creamos anteriormente e insertamos la nueva zona y los registros para dicha zona
~~~
INSERT INTO domains (name, type) VALUES ('prueba.org', 'MASTER');
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'prueba.org', 'nodo1.prueba.org nodo1.prueba.org 1', 'SOA', 86400, NULL);
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'prueba.org', 'nodo1.prueba.org', 'NS', 86400, NULL);
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'prueba.org', 'nodo2.prueba.org', 'NS', 86400, NULL);
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'nodo1.prueba.org', '192.168.1.147', 'A', 86400, NULL);
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'nodo2.prueba.org', '192.168.1.137', 'A', 86400, NULL);
~~~

Para realizar la transferencia de zonas, tenemos que indicar a la tabla `supermasters` la ip del servidor esclavo y la zona que queremos transferir
~~~
MariaDB [powerdns]> INSERT INTO supermasters (ip, nameserver, account) VALUES ('192.168.1.137', 'nodo2.prueba.org', 'admin');
~~~

**Configuración Nodo 2 (Esclavo)**

Editamos el fichero de configuración /etc/powerdns/pdns.conf
~~~
 allow-axfr-ips=192.168.1.0/24
 config-dir=/etc/powerdns
 default-soa-name=nodo2.prueba.org
 disable-axfr=yes
 include-dir=/etc/powerdns/pdns.d
 launch=
 local-address=192.168.1.137
 local-port=53
 loglevel=4
 master=no
 setgid=pdns
 setuid=pdns
 slave=yes
 slave-cycle-interval=5
~~~

Tras realizar las configuraciones reiniciamos el servicio de pdns en ambos servidores
~~~
root@nodo1:/etc/powerdns# systemctl restart pdns
root@nodo2:/etc/powerdns# systemctl restart pdns
~~~

Para realizar la transferencia de zonas, tenemos que indicar a la tabla `supermasters` la ip del servidor maestro y la zona que queremos transferir
~~~
MariaDB [powerdns]> INSERT INTO supermasters (ip, nameserver, account) VALUES ('192.168.1.147', 'nodo1.prueba.org', 'admin');
~~~

Finalmente incrementamos el serial en el maestro `nodo1` para que se complete la transferencia de zonas
~~~
MariaDB [powerdns]> UPDATE records SET content = 'nodo1.prueba.org nodo1.prueba.org 3' WHERE type = 'SOA' AND name = 'prueba.org';
~~~

Comprobamos el fichero de log de `nodo2`
~~~
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 Created new slave zone 'prueba.org' from supermaster 192.168.1.147
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 1 slave domain needs checking, 0 queued for AXFR
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 Received serial number updates for 1 zone, had 0 timeouts
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 Domain 'prueba.org' is stale, master serial 3, our serial 0
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 Initiating transfer of 'prueba.org' from remote '192.168.1.147'
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 Starting AXFR of 'prueba.org' from remote 192.168.1.147:53
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 AXFR started for 'prueba.org'
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 AXFR of 'prueba.org' from remote 192.168.1.147:53 done
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 Backend transaction started for 'prueba.org' storage
May 28 16:30:50 stretch pdns_server[7094]: May 28 16:30:50 AXFR done for 'prueba.org', zone committed with serial number 3
May 28 16:30:55 stretch pdns_server[7094]: May 28 16:30:55 No new unfresh slave domains, 0 queued for AXFR already, 0 in progress
~~~

Comprobamos que ambos servidores resuelven nombres correctamente
**Nodo 1**
~~~
josemaria@daryl:~$ dig @192.168.1.147 ns prueba.org

; <<>> DiG 9.11.5-P4-5.1+deb10u1-Debian <<>> @192.168.1.147 ns prueba.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3497
;; flags: qr aa rd; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 3
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1680
;; QUESTION SECTION:
;prueba.org.			IN	NS

;; ANSWER SECTION:
prueba.org.		86400	IN	NS	nodo1.prueba.org.
prueba.org.		86400	IN	NS	nodo2.prueba.org.

;; ADDITIONAL SECTION:
nodo1.prueba.org.	86400	IN	A	192.168.1.147
nodo2.prueba.org.	86400	IN	A	192.168.1.137

;; Query time: 6 msec
;; SERVER: 192.168.1.147#53(192.168.1.147)
;; WHEN: jue may 28 16:32:53 CEST 2020
;; MSG SIZE  rcvd: 111
~~~

**Nodo 2**
~~~
josemaria@daryl:~$ dig @192.168.1.137 ns prueba.org

; <<>> DiG 9.11.5-P4-5.1+deb10u1-Debian <<>> @192.168.1.137 ns prueba.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3620
;; flags: qr aa rd; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 3
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1680
;; QUESTION SECTION:
;prueba.org.			IN	NS

;; ANSWER SECTION:
prueba.org.		86400	IN	NS	nodo1.prueba.org.
prueba.org.		86400	IN	NS	nodo2.prueba.org.

;; ADDITIONAL SECTION:
nodo1.prueba.org.	86400	IN	A	192.168.1.147
nodo2.prueba.org.	86400	IN	A	192.168.1.137

;; Query time: 13 msec
;; SERVER: 192.168.1.137#53(192.168.1.137)
;; WHEN: jue may 28 16:33:42 CEST 2020
;; MSG SIZE  rcvd: 111
~~~

## 4.3 Configuración con backend LDAP

**Instalación y configuración de LDAP**

Primero instalamos los paquetes necesarios
~~~
root@pdnsldap:~# apt install slapd ldap-utils -y 
~~~

Nos pedirá una contraseña para poder administrar LDAP

![Captura](/PowerDNS+LDAP/captura.png)

Tras introducirla, nos volverá a pedir que la introduzcamos para confirmarla
![Captura2](/PowerDNS+LDAP/captura2.png)

Pasamos a la configuración de LDAP temos que ejecutar lo siguiente
~~~
root@pdnsldap:~# dpkg-reconfigure -plow slapd
~~~

Primero nos preguntará si queremos configurar el servidor de OpenLDAP decimos que no
![Captura3]((/PowerDNS+LDAP/captura3.png)

Configuramos nuestro nombre de dominio, en mi caso será el siguiente
![Captura4](/PowerDNS+LDAP/captura4.png)

Indicaremos también nuestra organización

![Captura5](/PowerDNS+LDAP/captura5.png)

Indicaremos la contraseña del administrador, ésta contraseña será la misma que utilizamos anteriormente tras instalar el paquete `slapd`
![Captura6](/PowerDNS+LDAP/captura6.png)

También tendremos que indicar el motor de base de datos. Dejaremos el que nos indica por defecto
![Captura7](/PowerDNS+LDAP/captura7.png)

Nos preguntará si queremos que se elimine el directorio de slapd tras desinstalar el paquete slapd, indicaremos que sí lo elimine
![Captura8](/PowerDNS+LDAP/captura8.png)

Finalmente dejaremos la última opción por defecto
![Captura9](/PowerDNS+LDAP/captura9.png)

Comprobamos que todo está configurado correctamente
~~~
root@pdnsldap:~# slapcat 
dn: dc=josemaria,dc=gonzalonazareno,dc=org
objectClass: top
objectClass: dcObject
objectClass: organization
o: IESGN
dc: josemaria
structuralObjectClass: organization
entryUUID: 844889be-2d74-103a-9a7e-9ddfb26c0c73
creatorsName: cn=admin,dc=josemaria,dc=gonzalonazareno,dc=org
createTimestamp: 20200518165818Z
entryCSN: 20200518165818.302032Z#000000#000#000000
modifiersName: cn=admin,dc=josemaria,dc=gonzalonazareno,dc=org
modifyTimestamp: 20200518165818Z

dn: cn=admin,dc=josemaria,dc=gonzalonazareno,dc=org
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: LDAP administrator
userPassword:: e1NTSEF9bGNHTlFnRnNFNVE3TlVCZ2N5cC9RZW5Ea3o5Ry8yL0Q=
structuralObjectClass: organizationalRole
entryUUID: 8448a0c0-2d74-103a-9a7f-9ddfb26c0c73
creatorsName: cn=admin,dc=josemaria,dc=gonzalonazareno,dc=org
createTimestamp: 20200518165818Z
entryCSN: 20200518165818.302653Z#000000#000#000000
modifiersName: cn=admin,dc=josemaria,dc=gonzalonazareno,dc=org
modifyTimestamp: 20200518165818Z
~~~

Ahora vamos a añadir los índices necesarios para LDAP para poder añadir los registros de DNS. Tendremos que crear o modificar un fichero en /etc/ldap/slapd.conf 
Antes de realizar la configuración, pararemos el servicio de ldap
~~~
root@pdnsldap:/etc/ldap# systemctl stop slapd
~~~

Creamos o modificamos el fichero
~~~
index aRecord            pres,eq
index associatedDomain   pres,eq,sub
~~~

Actualizamos los índices
~~~
root@pdnsldap:/etc/ldap# sudo -u openldap slapindex
~~~

Y finalmente iniciamos el servicio de LDAP
~~~
root@pdnsldap:/etc/ldap# systemctl restart slapd
~~~

Vamos a crear un fichero de configuración con extensión .ldif en el que vamos a crear una nueva unidad organizativa, y crearemos los registros necesarios para que powerdns pueda consultar LDAP
~~~
dn: ou=dns,dc=josemaria,dc=gonzalonazareno,dc=org
objectclass: organizationalUnit
ou: dns

dn: dc=prueba,ou=dns,dc=josemaria,dc=gonzalonazareno,dc=org
objectclass: dnsdomain
objectclass: domainrelatedobject
dc: prueba
soarecord: pdnsldap.prueba.org root@jm.root.mail 0 1800 3600 604800 84600
nsrecord: pdnsldap.prueba.org
mxrecord: 10 mail.prueba.org
associateddomain: prueba.org

dn: dc=pdnsldap,ou=dns,dc=josemaria,dc=gonzalonazareno,dc=org
objectclass: top
objectclass: dnsdomain
objectclass: domainrelatedobject
dc: pdnsldap
arecord: 192.168.1.109
associateddomain: pdnsldap.prueba.org
~~~

Cargamos la configuración a nuestro LDAP
~~~
root@pdnsldap:~# ldapadd -x -D cn=admin,dc=josemaria,dc=gonzalonazareno,dc=org -W -f config.ldif 
Enter LDAP Password: 
adding new entry "ou=dns,dc=josemaria,dc=gonzalonazareno,dc=org"

adding new entry "dc=prueba,ou=dns,dc=josemaria,dc=gonzalonazareno,dc=org"

adding new entry "dc=pdnsldap,ou=dns,dc=josemaria,dc=gonzalonazareno,dc=org"
~~~

**Instalación y configuración de PowerDNS**

Instalamos powerdns y el backend de ldap
~~~
root@pdnsldap:~# apt install pdns-server pdns-backend-ldap -y 
~~~

Creamos un nuevo fichero para que powerdns establezca la conexión con LDAP. Creamos el fichero en /etc/powerdns/pdns.d/`nombrefichero.conf`
~~~
ldap-host=192.168.1.109
ldap-basedn=ou=dns,dc=josemaria,dc=gonzalonazareno,dc=org
ldap-method=strict
~~~

Editamos el fichero de configuración de powerdns /etc/powerdns/pdns.conf
~~~
config-dir=/etc/powerdns
default-soa-mail=jm.root.mail
default-soa-name=pdnsldap.prueba.org
disable-axfr=no
include-dir=/etc/powerdns/pdns.d
launch=ldap
local-port=53
local-address=192.168.1.109
log-dns-details=yes
loglevel=4
master=yes
query-local-address=0.0.0.0
setgid=pdns
setuid=pdns
slave=no
~~~

Reiniciamos el servicio de LDAP
~~~
root@pdnsldap:~# systemctl restart pdns.service 
~~~

Comprobamos finalmente que el servidor dns resuelve nombres
~~~
josemaria@daryl:~/Documentos/2ºASIR/proyecto$ dig @192.168.1.109 ns prueba.org

; <<>> DiG 9.11.5-P4-5.1+deb10u1-Debian <<>> @192.168.1.109 ns prueba.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 43558
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1680
;; QUESTION SECTION:
;prueba.org.			IN	NS

;; ANSWER SECTION:
prueba.org.		3600	IN	NS	pdnsldap.prueba.org.

;; ADDITIONAL SECTION:
pdnsldap.prueba.org.	3600	IN	A	192.168.1.109

;; Query time: 10 msec
;; SERVER: 192.168.1.109#53(192.168.1.109)
;; WHEN: jue may 28 21:44:53 CEST 2020
;; MSG SIZE  rcvd: 78
~~~

## 4.4 Configuración de PowerAdmin
Editamos el fichero /etc/mysql/mariadb.conf.d/50-server.cnf para que nuestra base de datos escuche desde todas las ips. Comentamos la línea bind-address
~~~
#bind-address = 127.0.0.1
~~~

Reiniciamos el servicio de mariadb para que se apliquen los cambios
~~~
root@nodo1:/etc/powerdns# vim /etc/mysql/mariadb.conf.d/50-server.cnf 
~~~

Como no tenemos la base de datos en local, tendrá que consultarla de manera externa. El usuario en la base de datos tendrá que tener los privilegios para que escuche peticiones de todas las ips para ello en el nodo en el que está corriendo nuestra base de datos ejecutamos.
~~~
MariaDB [(none)]> grant all on powerdns.* to 'powerdns'@'%' identified by 'powerdns';
Query OK, 0 rows affected (0.01 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.00 sec)
~~~

Tenemos que instalar los repositorios necesarios 
~~~
[root@poweradmin ~]# yum -y install epel-release
[root@poweradmin ~]# yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
~~~

Tenemos los repositorios necesarios, ahora necesitamos instalar los paquetes necesarios
~~~
[root@poweradmin ~]# yum -y install httpd php php-devel php-gd php-imap php-ldap php-mysql php-odbc php-pear php-xml php-xmlrpc php-mbstring php-mcrypt php-mhash gettext
~~~

Instalamos los paquetes PHP PEAR
~~~
[root@poweradmin ~]# yum -y install php-pear-DB php-pear-MDB2-Driver-mysqli
~~~

Iniciamos e habilitamos el servicio httpd
~~~
[root@poweradmin ~]# systemctl start httpd
[root@poweradmin ~]# systemctl enable httpd
~~~

Descargamos poweradmin en /var/www/html. Una vez descargado, lo descomprimiremos
~~~
[root@poweradmin html]# wget http://downloads.sourceforge.net/project/poweradmin/poweradmin-2.1.7.tgz
[root@poweradmin html]# tar xvf poweradmin-2.1.7.tgz 
~~~

Cambiaremos el nombre del directorio descomprimido para una mejor lectura
~~~
[root@poweradmin html]# mv poweradmin-2.1.7 poweradmin
~~~

**Configuración**

`Accederemos en nuestro navegador web a => ipmáquina/poweradmin/install`
Seleccionamos el idioma
![Captura](/poweradmin/captura.png)

Nos indicará que poweradmin podrá sobreescribir tablas y zonas creadas y podrán ser eliminadas. Por tanto nos hace la recomendación de que tengamos una copia de seguridad de nuestra base de datos
![Captura2](/poweradmin/captura2.png)

Y añadimos los parámetros con los cuales hemos configurado la base de datos. También asignaremos la contraseña de administrador de poweradmin
![Captura3](/poweradmin/captura3.png)

Añadimos el usuario y su respectiva contraseña para poweradmin y indicamos cuales serán el servidor primario y secundario
![Captura4](/poweradmin/captura4.png)

Tendremos que asignar los siguientes privilegios al usuario que hemos creado anteriormente para poweradmin
~~~
MariaDB [(none)]> GRANT SELECT, INSERT, UPDATE, DELETE ON powerdns.* TO 'root'@'192.168.1.147' IDENTIFIED BY 'root';
Query OK, 0 rows affected (0.00 sec)
~~~

![Captura](/poweradmin/captura5.png)

Ahora tenemos que modificar el fichero inc/config.inc.php y añadimos los parámetros de nuestra base de datos
~~~
$db_host		= '192.168.1.147';
$db_user		= 'powerdns';
$db_pass		= 'powerdns';
$db_name		= 'powerdns';
$db_type		= 'mysql';
$db_layer		= 'PDO';

$session_key		= '^dS!!YqDSGadC=SuU+^BGsLBT$]-Ybn24QJOjwQC=C99d]';

$iface_lang		= 'en_EN';

$dns_hostmaster		= 'nodo1.prueba.org';
$dns_ns1		= 'nodo1.prueba.org';
$dns_ns2		= 'nodo2.prueba.org';
~~~

Copiamos el fichero htaccess
~~~
[root@poweradmin poweradmin]# cp install/htaccess.dist .htaccess
~~~

Eliminamos el directorio **install**
~~~
[root@poweradmin poweradmin]# rm -rf install/
~~~

Tras esto hemos terminado la configuración de poweradmin. Accedemos a poweradmin con el usuario admin y la contraseña asignada a dicho usuario
![Captura](/poweradmin/captura6.png)

Vamos a añadir una nueva zona, nos dirigiremos a `Add master zone` y creamos la nueva zona
![Captura](/poweradmin/captura7.png)

A la zona creada le vamos a añadir registros para ello `List zones > View Zone(icono al lado de la papelera)`
![Captura](/poweradmin/captura8.png)

Comprobamos que la zona se ha generado correctamente realizando una consulta
~~~
josemaria@daryl:~/Documentos/2ºASIR/proyecto$ dig @192.168.1.147 ns prueba2.org

; <<>> DiG 9.11.5-P4-5.1+deb10u1-Debian <<>> @192.168.1.147 ns prueba2.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53238
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 3

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1680
;; QUESTION SECTION:
;prueba2.org.			IN	NS

;; ANSWER SECTION:
prueba2.org.		86400	IN	NS	nodo1.prueba.org.
prueba2.org.		86400	IN	NS	nodo2.prueba.org.

;; ADDITIONAL SECTION:
nodo2.prueba.org.	86400	IN	A	192.168.1.137
nodo1.prueba.org.	86400	IN	A	192.168.1.147

;; Query time: 7 msec
;; SERVER: 192.168.1.147#53(192.168.1.147)
;; WHEN: jue may 28 20:57:30 CEST 2020
;; MSG SIZE  rcvd: 119
~~~

Para que la zona se transfiera a nuestro servidor esclavo, tenemos que añadir la zona a la tabla supermasters para ello `Add supermasters`
![Captura](/poweradmin/captura9.png)

Y comprobamos que efectivamente nuestro servidor esclavo responde sin problemas
~~~
josemaria@daryl:~/Documentos/2ºASIR/proyecto$ dig @192.168.1.137 ns prueba2.org

; <<>> DiG 9.11.5-P4-5.1+deb10u1-Debian <<>> @192.168.1.137 ns prueba2.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3845
;; flags: qr aa rd; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 3
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1680
;; QUESTION SECTION:
;prueba2.org.			IN	NS

;; ANSWER SECTION:
prueba2.org.		86400	IN	NS	nodo2.prueba.org.
prueba2.org.		86400	IN	NS	nodo1.prueba.org.

;; ADDITIONAL SECTION:
nodo2.prueba.org.	86400	IN	A	192.168.1.137
nodo1.prueba.org.	86400	IN	A	192.168.1.147

;; Query time: 15 msec
;; SERVER: 192.168.1.137#53(192.168.1.137)
;; WHEN: jue may 28 20:57:52 CEST 2020
;; MSG SIZE  rcvd: 119
~~~

## 4.5 Delegación de subdominios
**Configuración Nodo 3**

Como hemos realizado en los nodos anteriores, vamos a instalar el servidor de base de datos mariadb
~~~
root@nodo3:~# apt install -y mariadb-server
~~~

Instalamos el servidor powerdns y el backend para mysql 
~~~
root@nodo3:~# apt install -y pdns-server pdns-backend-mysql 
~~~

`Nos aparecerá una ventana preguntando si queremos que pdns-backend-mysql configure la base de datos, seleccionamos no.`

Vamos a crear una base de datos, un usuario y asignaremos los privilegios necesarios sobre la base de datos creada
~~~
MariaDB [(none)]> create database powerdns;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> grant all on powerdns.* to 'powerdns'@'localhost' identified by 'powerdns';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.00 sec)
~~~

Cargamos el fichero con el esquema de powerdns para mariadb en la base de datos creada anteriormente
~~~
root@nodo3:~# mysql -u powerdns -p powerdns < /usr/share/dbconfig-common/data/pdns-backend-mysql/install/mysql
~~~

Editamos el fichero de configuración /etc/powerdns/pdns.d/pdns.local.gmysql.conf para establecer la comunicación entre powerdns y mariadb
~~~
# MySQL Configuration
#
# Launch gmysql backend
launch+=gmysql

# gmysql parameters
gmysql-host=localhost
gmysql-port=3306
gmysql-dbname=powerdns
gmysql-user=powerdns
gmysql-password=powerdns
gmysql-dnssec=yes
# gmysql-socket=
~~~

Editamos el fichero de configuración /etc/powerdns/pdns.conf
~~~
allow-axfr-ips=127.0.0.0/8,::1, 192.168.1.0/24
allow-recursion=192.168.1.0/24
config-dir=/etc/powerdns
disable-axfr=no
include-dir=/etc/powerdns/pdns.d
launch=
local-address=192.168.1.147
local-port=53
log-dns-details=yes
loglevel=4
master=yes
query-local-address=0.0.0.0
recursor=127.0.0.1:1553
setgid=pdns
setuid=pdns
slave=no
~~~

Accedemos a la base de datos que creamos anteriormente e insertamos la nueva zona y los registros para dicha zona
~~~
INSERT INTO domains (name, type) VALUES ('prueba.gn.org', 'MASTER');
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'prueba.gn.org', 'nodo3.prueba.gn.org nodo3.prueba.gn.org 1', 'SOA', 86400, NULL);
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'prueba.gn.org', 'nodo3.prueba.gn.org', 'NS', 86400, NULL);
INSERT INTO records (domain_id, name, content, type, ttl, prio) VALUES (1, 'nodo3.prueba.gn.org', '192.168.1.136', 'A', 86400, NULL);
~~~

Realizamos una consulta para comprobar que responde
~~~
josemaria@daryl:~/Documentos/2ºASIR/proyecto$ dig @192.168.1.136 nodo3.prueba.gn.org

; <<>> DiG 9.11.5-P4-5.1+deb10u1-Debian <<>> @192.168.1.136 nodo3.prueba.gn.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39001
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1680
;; QUESTION SECTION:
;nodo3.prueba.gn.org.		IN	A

;; ANSWER SECTION:
nodo3.prueba.gn.org.	86400	IN	A	192.168.1.136

;; Query time: 2 msec
;; SERVER: 192.168.1.136#53(192.168.1.136)
;; WHEN: jue may 28 20:31:07 CEST 2020
;; MSG SIZE  rcvd: 64
~~~

Vemos que funciona, ahora tenemos que instalar el paquete de powerdns recursor, ya que a través del nodo1 realizaremos consultas recursivas
~~~
root@nodo3:/etc/powerdns# apt install pdns-recursor -y
~~~

Modificamos el fichero de configuración /etc/powerdns/recursor.conf
~~~
allow-from=192.168.1.0/24
config-dir=/etc/powerdns
forward-zones=prueba.gn.org=192.168.1.136;
hint-file=/usr/share/dns/root.hints
local-address=0.0.0.0
local-port=1553
loglevel=4
query-local-address=0.0.0.0
setgid=pdns
setuid=pdns
~~~

Tenemos que modificar el fichero /etc/powerdns/pdns.conf en el **nodo1** y modificamos el siguiente parámetro
~~~
recursor=192.168.1.136:1553
~~~

Reiniciamos los servicios de powerdns en el `nodo 3`
~~~
root@nodo3:/etc/powerdns# systemctl restart pdns
root@nodo3:/etc/powerdns# systemctl restart pdns-recursor
~~~

Reiniciamos el servicio de powerdns en el `nodo 1`
~~~
root@nodo1:/etc/powerdns# systemctl restart pdns
~~~

Realizamos una consulta al `nodo 3` desde el `nodo 1`
~~~
josemaria@daryl:~/Documentos/2ºASIR/proyecto$ dig @192.168.1.147 nodo3.prueba.gn.org

; <<>> DiG 9.11.5-P4-5.1+deb10u1-Debian <<>> @192.168.1.147 nodo3.prueba.gn.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62977
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;nodo3.prueba.gn.org.		IN	A

;; ANSWER SECTION:
nodo3.prueba.gn.org.	85884	IN	A	192.168.1.136

;; Query time: 2 msec
;; SERVER: 192.168.1.147#53(192.168.1.147)
;; WHEN: jue may 28 20:28:33 CEST 2020
;; MSG SIZE  rcvd: 64
~~~

# 5. Conclusión 
**PowerDNS** es un potente servidor dns en el cuál tenemos diversas opciones de guardar nuestra información tanto en bases de datos como en ficheros de texto. Nos ofrece diversas opciones de configuración de motores de bases de datos con lo cuál tenemos amplia variedad de diversas configuraciones. Respecto a la hora de poder guardar la información en base de datos tenemos la posibilidad de guardar la información de forma redundante como la creación de clúster o replicación de dichas bases de datos. También podemos observar a la hora de la transferencia de zonas maestro/esclavo sólo tendríamos que incrementar el serial, respecto a BIND nos ahorramos tiempo y ficheros de configuración.
También nos ofrece la posibilidad de monitorizar los servicios y hacer colección de métricas mediante graphite y las mostraremos con carbon.  

# 6. Siguientes pasos
* Creación de clúster con mariadb
* Replicación de mariadb
* Configuración IPV6
* Monitorización
* Despliegue de PowerDNS a través de contenedores
* DNSDIST (balanceador de carga)

# 7. Webgrafía
* [Documentación servidor Autoritativo](https://doc.powerdns.com/authoritative/index.html)
* [Documentación servidor Recursivo](https://doc.powerdns.com/recursor/getting-started.html)
* [Todos los Backends](https://doc.powerdns.com/authoritative/backends/index.html)
* [Todos los webfrontends](https://github.com/PowerDNS/pdns/wiki/WebFrontends)